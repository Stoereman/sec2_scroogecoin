import java.util.ArrayList;
import java.util.HashSet;

public class TxHandler {
		private  UTXOPool utxoPool;
	/* Creates a public ledger whose current UTXOPool (collection of unspent 
	 * transaction outputs) is utxoPool. This should make a defensive copy of 
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public TxHandler(UTXOPool utxoPool) {
		this.utxoPool = utxoPool;
	}

	/* Returns true if 
	 * (1) all outputs claimed by tx are in the current UTXO pool, 
	 * (2) the signatures on each input of tx are valid, 
	 * (3) no UTXO is claimed multiple times by tx, 
	 * (4) all of tx’s output values are non-negative, and
	 * (5) the sum of tx’s input values is greater than or equal to the sum of   
	        its output values;
	   and false otherwise.
	 */

	public boolean isValidTx(Transaction tx) {
		UTXOPool uniqueUTXOList = new UTXOPool();
		double previousTxOutSum = 0;
		double currentTxOutSum = 0;

		for(int i = 0; i < tx.numInputs(); i++){
			Transaction.Input input = tx.getInput(i);
			UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);

			// * (1) all outputs claimed by tx are in the current UTXO pool
			if (!utxoPool.contains(utxo)) return false;
			// * (2) the signatures on each input of tx are valid
			Transaction.Output output = utxoPool.getTxOutput(utxo);
			RSAKey rsaKey = output.address;
			if (!rsaKey.verifySignature(tx.getRawDataToSign(i), input.signature)) return false;
			// * (3) no UTXO is claimed multiple times by tx,
			if (uniqueUTXOList.contains(utxo)) return false;

			// * (4) all of tx’s output values are non-negative, and
			uniqueUTXOList.addUTXO(utxo, output);
			previousTxOutSum += output.value; 
		}

		// * (5) the sum of tx’s input values is greater than or equal to the sum of
		//	        its output values;
		for(Transaction.Output output : tx.getOutputs()){
			if (output.value<=0) return false;
			currentTxOutSum += output.value;
		}
		return previousTxOutSum >= currentTxOutSum;
	}

	/* Handles each epoch by receiving an unordered array of proposed 
	 * transactions, checking each transaction for correctness, 
	 * returning a mutually valid array of accepted transactions, 
	 * and updating the current UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {
		HashSet<Transaction> transactions = new HashSet<>();
		for (Transaction tx : possibleTxs){
			if (isValidTx(tx)){
				transactions.add(tx);
				for (Transaction.Input input : tx.getInputs()){
					UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);
					utxoPool.removeUTXO(utxo);
				}
				for (int i = 0; i<tx.numOutputs(); i++){
					Transaction.Output output = tx.getOutput(i);
					UTXO utxo = new UTXO(tx.getHash(), i);
					utxoPool.addUTXO(utxo,output);
				}
			}
		}
		Transaction[] validTransactions = new Transaction[transactions.size()];
		return transactions.toArray(validTransactions);
	}

} 
